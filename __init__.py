import json
import leds
import math
import network
import time
import urequests
from st3m.application import Application, ApplicationContext
from st3m.goose import Enum
import st3m.run

# Set to True to use faked train locations (taken from track points) instead of the real API.
STUB_API = False

API_URL = "https://api.c3toc.de/trains.json"
SCALE_F = 200
FETCH_INTERVAL = 500 if STUB_API else 10000
NUM_LEDS = 40
TRACK_COLOR = [0.5, 0.8, 1, 0.6]
STATION_COLOR = [0.5, 0.8, 1, 0.8]
TRAIN_COLORS = [
    [63 / 255, 255 / 255, 33 / 255],
    [251 / 255, 72 / 255, 196 / 255],
    [0.7, 0.7, 0.7],
]

class ViewState(Enum):
    STARTING = 1
    MAIN = 2
    ROTATING = 3
    WILL_FETCH_1 = 4
    WILL_FETCH_2 = 5
    FETCHING = 6

class C3Trains(Application):
    def __init__(self, app_ctx):
        super().__init__(app_ctx)
        self.view_state = ViewState.STARTING
        self.t = 0
        self.last_fetch_t = None
        self.online = False
        self.display_rotation = 0
        self.target_display_rotation = 0
        self.track_points = []
        self.stations = []
        self.trains = []
        bundle_path = app_ctx.bundle_path
        if bundle_path == "":
            bundle_path = "/flash/sys/apps/noniq-c3trains"
        self.read_tracks(f"{bundle_path}/tracks.json")
        self.read_stations(f"{bundle_path}/tracks.json")


    def draw(self, ctx):
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        leds.set_all_rgb(0, 0, 0)

        if self.view_state == ViewState.ROTATING and math.fabs(self.display_rotation - self.target_display_rotation) < 0.1:
            self.view_state = ViewState.MAIN
            self.display_rotation = self.target_display_rotation
        ctx.rotate(self.display_rotation)

        ctx.text_align = ctx.CENTER
        ctx.font = "Camp Font 1"
        ctx.font_size = 30
        ctx.rgb(251 / 255, 72 / 255, 196 / 255).move_to(0, -62).text("c3trains")

        ctx.font = "Material Icons"
        ctx.font_size = 16
        ctx.move_to(0, 113)
        if self.online:
            ctx.rgb(0.5, 0.5, 0.5).text("\ue63e")
        else:
            for i in range((self.t // 1000 % 2) * 4, 40, 8):
                leds.set_rgb(i, 1, 0, 0)
            ctx.rgb(1.0, 0.3, 0.3).text("\ue648")

        # draw progress bar until new fetch
        if self.last_fetch_t != None:
            ctx.save()
            ctx.line_width = 4
            a1 = math.pi / 2
            a2 = a1 + math.pi * 2 * min(1, max(0, 1 - (self.t - self.last_fetch_t) / FETCH_INTERVAL))
            if a1 != a2:
                ctx.rgb(0.25, 0.25, 0.25).begin_path().arc(0, 0, 118, max(a1, a2), min(a1, a2), 1).stroke()
            ctx.restore()

        # draw track
        for i, points in enumerate(self.track_points):
            ctx.rgba(*TRACK_COLOR).begin_path().move_to(points[0][0], points[0][1])
            for p in points[1:]:
                ctx.line_to(p[0], p[1])
            ctx.line_width = 2
            ctx.stroke()

        # draw stations
        for station in self.stations:
            ctx.rgba(*STATION_COLOR).arc(station[0], station[1], 2.5, 0, 2 * math.pi, 1).fill()

        if self.last_fetch_t != None:
            # draw trains (and set leds)
            ctx.font = "Camp Font 3"
            ctx.font_size = 17
            for i, train in enumerate(self.trains):
                color = self.get_train_color(train["name"])
                color = TRAIN_COLORS[i]
                if color is None:
                    color = TRAIN_COLORS[-1]
                ctx.rgb(*color).arc(train["x"] - 2, train["y"] - 2, 4, 0, 2 * math.pi, 1).fill()
                ctx.move_to(0, 68 + 20 * i).rgb(*color).text(train["name"])
                led_index = (round(math.atan2(train["y"], train["x"]) / (math.pi * 2) * NUM_LEDS) + NUM_LEDS // 4)
                if self.target_display_rotation == math.pi:
                    led_index += NUM_LEDS // 2
                leds.set_rgb(led_index % NUM_LEDS, *color)

        if self.view_state == ViewState.WILL_FETCH_2:
            self.view_state = ViewState.FETCHING

        if self.view_state == ViewState.WILL_FETCH_1 or self.view_state == ViewState.WILL_FETCH_2 or self.view_state == ViewState.FETCHING:
            ctx.save()
            ctx.font = "Material Icons"
            ctx.font_size = 50
            ctx.rgba(1, 1, 1, 0.3).arc(0, 0, 30, math.pi * 2, 0, 1).fill().move_to(0, 25).rgba(0, 0, 0, 0.7).text("\uea5c")
            ctx.restore()
            if self.view_state == ViewState.WILL_FETCH_1:
                self.view_state = ViewState.WILL_FETCH_2

        leds.update()

    def think(self, ins, delta_ms):
        super().think(ins, delta_ms)
        self.t += delta_ms

        if self.view_state == ViewState.STARTING: # Make sure we do not block initial drawing on startup
            self.view_state = ViewState.MAIN
            return

        if ins.imu.acc[0] < -1.5 and self.target_display_rotation == 0:
            self.view_state = ViewState.ROTATING
            self.target_display_rotation = math.pi
        elif ins.imu.acc[0] > 1.5 and self.target_display_rotation == math.pi:
            self.view_state = ViewState.ROTATING
            self.target_display_rotation = 0
        if self.view_state == ViewState.ROTATING:
            rotation_increment = min(0.19, 0.05 * delta_ms)
            if self.target_display_rotation < self.display_rotation:
                rotation_increment *= -1
            self.display_rotation += rotation_increment

        self.online = network.WLAN(network.STA_IF).isconnected()
        if not self.online and not STUB_API:
            return

        if self.view_state == ViewState.MAIN and (self.last_fetch_t == None or self.t > self.last_fetch_t + FETCH_INTERVAL):
            self.view_state = ViewState.WILL_FETCH_1

        if self.view_state == ViewState.FETCHING:
            started = time.time()
            if STUB_API:
                self.fetch_train_locations_from_stubbed_api()
            else:
                self.fetch_train_locations()
            self.last_fetch_t = self.t + (time.time() - started) * 1000
            self.view_state = ViewState.MAIN


    def read_tracks(self, path):
        self.tracks = []
        with open(path) as f:
            j = json.load(f)
            for (name, data) in j['tracks'].items():
                trackpoints = [[p['lat'], p['lon']] for p in data["points"]]
                self.tracks.append(trackpoints)

            all_points = [item for sublist in self.tracks for item in sublist]
            self.min_lat = min([p[0] for p in all_points])
            self.max_lat = max([p[0] for p in all_points])
            self.min_lon = min([p[1] for p in all_points])
            self.max_lon = max([p[1] for p in all_points])
            self.scale = min(SCALE_F / (self.max_lat - self.min_lat), SCALE_F / (self.max_lon - self.min_lon))
            self.track_points = []
            for i, trackpoints in enumerate(self.tracks):
                display_trackpoints = [self.geo_coords_to_pixels(p[0], p[1]) for p in trackpoints]
                self.track_points.append(display_trackpoints)


    def read_stations(self, path):
        self.stations = []
        with open(path) as f:
            j = json.load(f)
            for (name, data) in j['waypoints'].items():
                if data["type"] == "Bf" or data["type"] == "Hp":
                    self.stations.append(self.geo_coords_to_pixels(data["lat"], data["lon"]))
        print(self.stations)

    def geo_coords_to_pixels(self, lat, lon):
        return [(lon - self.min_lon - (self.max_lon - self.min_lon) / 2) * self.scale, -(lat - self.min_lat - (self.max_lat - self.min_lat) / 2) * self.scale]


    def get_train_color(self, name):
        if name == "Bummelbahn":
            return [63 / 255, 255 / 255, 33 / 255]
        elif name == "Trans-Habitat-Exp":
            return [251 / 255, 72 / 255, 196 / 255]
        else:
            return [0.5, 0.5, 0.5]


    def fetch_train_locations_from_stubbed_api(self):
        self.trains.clear()
        all_points = [item for sublist in self.track_points for item in sublist]
        trackpoint = all_points[(self.t // 250) % len(all_points)]
        self.trains.append({"name": "Ghosttrain", "x": trackpoint[0], "y": trackpoint[1]})
        trackpoint = all_points[(self.t // 500) % len(all_points)]
        self.trains.append({"name": "404 Express", "x": trackpoint[0], "y": trackpoint[1]})


    def fetch_train_locations(self):
        print("Fetching train locations")
        res = None
        data = None
        try:
            res = urequests.get(API_URL)
            data = res.json()["trains"]
        except:
            pass
        if data == None:
            if res != None:
                print(f"Invalid JSON or network error: {res.status_code}: {res.text}")
            else:
                print("Network error")
        else:
            print(data)
            self.trains.clear()
            for name, data in data.items():
                xy = self.geo_coords_to_pixels(data["lat"], data["lon"])
                self.trains.append({"name": name, "x": xy[0], "y": xy[1]})
            self.trains.reverse()
            print(self.trains)


if __name__ == '__main__':
    st3m.run.run_view(C3Trains(ApplicationContext()))
